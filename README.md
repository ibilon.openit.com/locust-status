# Locust Status

Load testing for uploading license status files to the Core Server.

## Install

```sh
git clone git@gitlab.com:ibilon.openit.com/locust-status.git
cd locust-status
python -m pip install -r requirements.txt
```

## Run

Open terminal and `cd` to `locust-status` repository.

Run a master node.

```sh
locust --master
```

Run a worker node.

```sh
locust --worker --master-host <master hostname>
```
