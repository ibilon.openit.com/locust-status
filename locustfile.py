from string import ascii_uppercase, digits
from locust import HttpUser, task
from multidict import MultiDict
from random import choice
from pathlib import Path

payloadfile = Path('payload.txt')
payload     = payloadfile.read_text()
files       = MultiDict([ ('file[]', payload) for i in range(1) ])

class Client(HttpUser):
    @task
    def upload_licpoll(self):
        self.client.post('/api/data?type=status&product=locust', files=files, verify=False)
